import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges
} from '@angular/core';
import { ColumnFieldType } from '../../types/column-field.type';
import { DataTableType } from '../../types/data-table.type';
import { PageChangeEventType } from '../../types/page-change-event.type';
import { RowActionWithData } from '../../types/row-actions.type';

@Component({
  selector: 'data-table',
  templateUrl: './data-table.component.html'
})
export class DataTableComponent<A> implements OnChanges {

  @Input()
  public tableConfig: DataTableType;

  @Input()
  public tableContent: A[];

  @Output()
  public getDataForPage = new EventEmitter<PageChangeEventType>();

  @Output()
  public startRowAction = new EventEmitter<{}>();

  public pageList: number[];

  public ngOnChanges(changes: SimpleChanges) {
    // const isColumnCountValid =
    // this.validateColumnSumIs12(changes.tableConfig.currentValue.columns);
    this.pageList = Array(Math.ceil(this.tableConfig.totalEntryCount / this.tableConfig.entriesPerPage))
      .fill(0)
      .map((x, i) => i + 1);
  }

  private validateColumnSumIs12(columnDefinitions: ColumnFieldType[]): boolean {
    return columnDefinitions.reduce((aggregate, col) => aggregate + col.width, 0) === 12;
  }

  public changePageTo(pageNumber: number): void {
    const eventData: PageChangeEventType = {
      fromEntry: (pageNumber - 1) * this.tableConfig.entriesPerPage,
      toEntry: (pageNumber * this.tableConfig.entriesPerPage),
    };

    this.getDataForPage.emit(eventData);
  }

  public onRowActionClicked(actionType: string, rowData: A): void {
    const userAction: RowActionWithData<A> = {
      actionToPerform: actionType,
      rowData: rowData
    };

    this.startRowAction.emit(userAction);
  }
}
