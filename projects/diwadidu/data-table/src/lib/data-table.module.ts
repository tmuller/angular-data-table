import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DataTableComponent } from './components/data-table/data-table.component';
import { PaginationComponent } from './components/pagination/pagination.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    DataTableComponent,
    PaginationComponent,
  ],
  exports: [DataTableComponent]
})
export class DataTableModule {
}
