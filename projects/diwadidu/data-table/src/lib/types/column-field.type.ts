export interface ColumnFieldType {
  title: string;
  width: number;
  dataField: string;
}
