import { ColumnFieldType } from './column-field.type';
import { RowActionsType } from './row-actions.type';

export interface DataTableType {
  columns: ColumnFieldType[];
  entriesPerPage: number;
  totalEntryCount: number;
  rowActions: RowActionsType[];
}
