export interface PageChangeEventType {
  fromEntry: number;
  toEntry: number;
}
