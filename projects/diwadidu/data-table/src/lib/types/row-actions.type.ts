export interface RowActionsType {
  actionDescription: string;
  logoImageUrl: string;
  actionToPerform: string;
  showOption: (x) => boolean;
}

export interface RowActionWithData<T> {
  actionToPerform: string;
  rowData: T;
}
