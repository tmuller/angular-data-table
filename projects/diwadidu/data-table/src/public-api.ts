/*
 * Public API Surface of data-table
 */

// export * from './lib/generic-table.service';
export * from './lib/components/data-table/data-table.component';
export * from './lib/data-table.module';
export * from './lib/types/data-table.type';
export * from './lib/types/page-change-event.type';
