import { Component, OnInit } from '@angular/core';
import { DataTableType, PageChangeEventType } from '@diwadidu/data-table';
import { EmployeeType } from '../../types/employee.type';

@Component({
  selector: 'employees-component',
  templateUrl: './employees.component.html'
})
export class EmployeesComponent implements OnInit {

  public employeeData = {
    tableData: []
  };

  public employeeTableConfig: DataTableType = {
    columns: [
      {
        title: 'Name',
        width: 3,
        dataField: 'userName'
      },
      {
        title: 'Job',
        width: 2,
        dataField: 'jobTitle'
      },
      {
        title: 'Years of Service',
        width: 2,
        dataField: 'yearsOfService'
      },
      {
        title: 'Age',
        width: 3,
        dataField: 'employeeAge'
      },
      {
        title: '',
        width: 2,
        dataField: 'ROW_ACTIONS'
      },
    ],
    entriesPerPage: 3,
    totalEntryCount: sampleData.length,
    rowActions: [
      {
        actionDescription: 'Delete',
        logoImageUrl: '',
        actionToPerform: 'DELETE',
        showOption: (x: EmployeeType) => x.yearsOfService > 4,
      },
      {
        actionDescription: 'Edit',
        logoImageUrl: '',
        actionToPerform: 'EDIT',
        showOption: (x: EmployeeType) => true,
      },
      {
        actionDescription: 'Copy',
        logoImageUrl: '',
        actionToPerform: 'COPY',
        showOption: (x: EmployeeType) => x.employeeAge > 34,
      },
    ],
  };

  public ngOnInit() {
    this.paginateDataFromTo({fromEntry: 0, toEntry:this.employeeTableConfig.entriesPerPage})
  }

  public paginateDataFromTo(pageInfo: PageChangeEventType): void {
    this.employeeData.tableData = sampleData.slice(pageInfo.fromEntry, pageInfo.toEntry);
  }

  public executeRowAction(eventData): void {
    console.log('user clicked on an action for ' + eventData.rowData.userName, eventData);
  }
}

const sampleData = [
  {
    userName: 'Jennifer',
    jobTitle: 'Architect',
    yearsOfService: 10,
    employeeAge: 42,
  },
  {
    userName: 'Richard',
    jobTitle: 'Developer',
    yearsOfService: 3,
    employeeAge: 32,
  },
  {
    userName: 'Beth',
    jobTitle: 'Developer',
    yearsOfService: 5,
    employeeAge: 37,
  },
  {
    userName: 'Julie',
    jobTitle: 'Junior Developer',
    yearsOfService: 2,
    employeeAge: 25,
  },
  {
    userName: 'Peter',
    jobTitle: 'Developer',
    yearsOfService: 12,
    employeeAge: 33,
  },
  {
    userName: 'Mathilda',
    jobTitle: 'Junior Developer',
    yearsOfService: 2,
    employeeAge: 23,
  },
  {
    userName: 'Boss Man',
    jobTitle: 'Chief of Development',
    yearsOfService: 25,
    employeeAge: 52,
  },
];
