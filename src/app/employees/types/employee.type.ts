export interface EmployeeType {
  userName: string,
  jobTitle: string,
  yearsOfService: number,
  employeeAge: number
}
